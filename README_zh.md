# vendor/hisilicon 组件<a name="ZH-CN_TOPIC_0000001130275863"></a>

- [简介](#section469617221261)
- [约束](#section12212842173518)
- [相关仓库](#section641143415335)
- [Hi3861V100实验开发指导](#ZH-CN_TOPIC_0000001130176841)
- [Hi3861V100硬件介绍](#section11660541593)
- [开发环境搭建](#section11660541593)
- [WiFi_IoT基础控制实验](#section11660541593)
- [附录](#section11660541593)

## 简介<a name="section469617221261"></a>

参考开发板，编译框架适配、解决方案参考代码和脚本。

## 约束<a name="section12212842173518"></a>

支持HiSpark\_taurus（Hi3516DV300） 、HiSpark\_aries（Hi3518EV300）、HiSpark\_pegasus（Hi3861V100）。

## Hi3861V100实验开发指导<a name="ZH-CN_TOPIC_0000001130176841"></a>

前言：在学习实验之前，请学习一下理论知识，[前往理论知识课程](http://developer.huawei.com/consumer/cn/training/course/introduction/C101641968823265204)

## Hi3861V100硬件介绍<a name="section11660541593"></a>

-    [Hi3861V100硬件介绍](http://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-lite-introduction-hi3861.md)

## 开发环境搭建<a name="section11660541593"></a>

-    第一种方式(推荐使用)：
     - [源码获取](https://device.harmonyos.com/cn/docs/documentation/guide/sourcecode-acquire-0000001050769927)
     - [搭建IDE工具开发环境](https://device.harmonyos.com/cn/docs/documentation/guide/ide-install-windows-ubuntu-0000001194073744)
     - [打开工程/源码](https://device.harmonyos.com/cn/docs/documentation/guide/open_project-0000001071680043)
     - [编译Hi3861V100开发板源码(由于Hi3861V100与Hi3516DV300源码相同，为方便开发请选择Ubuntu方式编译)](https://device.harmonyos.com/cn/docs/documentation/guide/ide-hi3861v100-compile-0000001192526021)
     - [Hi3861V100开发板烧录](https://device.harmonyos.com/cn/docs/documentation/guide/ide-hi3861-upload-0000001051668683)
     - [运行第一个程序Hello world](http://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-lite-steps-hi3861-helloworld.md)
-    第二种方式(hb方式)：
     - [源码获取](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-lite-sourcecode-acquire.md)
     - [搭建Hi3861V100开发环境](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-lite-steps-hi3861-setting.md)
     - [编译Hi3861V100开发板源码(选择安装包方式搭建环境)](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-lite-steps-hi3861-building.md)
     - [Hi3861V100开发板烧录](http://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-lite-steps-hi3861-burn.md)
     - [运行第一个程序Hello world](http://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-lite-steps-hi3861-helloworld.md)
     - [环境搭建FAQ](http://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-lite-steps-hi3861-faqs.md)


## WiFi_IoT基础控制实验<a name="section11660541593"></a>

- [led实验](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/guide/device-wlan-led-control.md)

## 附录<a name="section11660541593"></a>

- [更多请访问官网](https://www.hisilicon.com/cn/products/smart-iot/ShortRangeWirelessIOT/Hi3861V100)
## 相关仓库<a name="section641143415335"></a>

- **vendor_hisilicon**

- [device_soc_hisilicon](https://gitee.com/openharmony/device_soc_hisilicon)

- [device_board_hisilicon](https://gitee.com/openharmony/device_board_hisilicon)

- [device_hisilicon_third_party_uboot](https://gitee.com/openharmony/device_hisilicon_third_party_uboot)

